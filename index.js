//Importar la clase express
import express from 'express';

//Crear un objeto express
const app=express();
const puerto=3000;

//Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al Mundo de Backend!!!")
});

//Ruta Sumar

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta restar

app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Multiplicar

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)*Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Modular

app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Dividir

app.get("/dividir", (peticion, respuesta)=>{

    if(b===0){
        let respuesta;
        respuesta.send("No se puede dividir el número en cero")
    
    }else {
        let resultado=Number(peticion.query.a)*Number(peticion.query.b);
    
        respuesta.send(resultado.toString());
    }

});

//Iniciar un servidor

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor")});
